import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../models/post.models';
import { PostsService } from '../services/posts.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  post: Post;
  title: string;
  content: string;
  loveIts: number;
  createdAt: Date = new Date();
  posts: Post[];



  constructor(private postsService: PostsService) {
    this.postsService.postsSubject.subscribe(posts => {
      this.posts = posts;
    });
   }

  onDeletePost(post: Post) {
    this.postsService.removePost(post);
  }

  ngOnInit() {
  }

  getDate() {

    return this.createdAt;
  }

  like(index: number) {

    this.postsService.likePost(index);
  }

  dislike(index: number) {

    this.postsService.dislikePost(index);
  }

}
