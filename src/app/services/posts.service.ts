import { Injectable } from '@angular/core';
import { Post } from '../models/post.models';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsSubject = new BehaviorSubject<Post[]>([new Post('Ici le titre du post n°1', 'Ici le contenu du post n°1', 1),
  new Post('Ici le titre du post n°2', 'Ici le contenu du post n°2', -1),
  new Post('Ici le titre du post n°3', 'Ici le contenu du post n°3', 0)]);

  constructor() {
  }

  emitPosts(posts: Post[]) {
    this.postsSubject.next(posts);
  }

  createNewPost(newPost: Post) {
    const posts = this.postsSubject.value;
    posts.push(newPost);
    this.emitPosts(posts);
  }

  likePost(index: number) {
    const posts = this.postsSubject.value;
    posts[index].loveIts ++;
    this.emitPosts(posts);

  }

  dislikePost(index: number) {
    const posts = this.postsSubject.value;
    posts[index].loveIts --;
    this.emitPosts(posts);

  }

  removePost(post: Post) {
    const posts = this.postsSubject.value;
    const postIndexToRemove = posts.findIndex(
      (postEl) => {
        if (postEl === post) {
          return true;
        }
      }
    );
    posts.splice(postIndexToRemove, 1);
    this.emitPosts(posts);
  }
}
