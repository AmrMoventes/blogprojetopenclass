import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostsService } from './services/posts.service';
import { Post } from './models/post.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'blog';
  postsSubscription: Subscription ;
  posts: Post[] = [];


  constructor(private postsService: PostsService) {
  }

  ngOnInit() {
    this.postsSubscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
  }


  onDeletePost(post: Post) {
    this.postsService.removePost(post);
  }


  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }

}
